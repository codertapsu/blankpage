// const canvas = document.getElementById("canvas"),
//   ctx = canvas.getContext("2d"),
//   W = canvas.width,
//   H = canvas.height,
//   gravity = .5,
//   bounceFactor = .7;

// class Ball {
//   constructor(x, y) {
//     this.x = x;
//     this.y = y;
//     this.radius = 15;
//     this.color = "blue";
//     this.vx = 0;
//     this.vy = 1
//   }
//   draw() {
//     ctx.beginPath();
//     ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2);
//     ctx.fillStyle = this.color;
//     ctx.fill();
//     ctx.closePath();
//   }

//   update() {
//     this.y += this.vy;
//     this.vy += gravity;
//     if (this.y + this.radius > H) {
//       this.y = H - this.radius;
//       this.vy *= (this.vy > 0.1) ? -bounceFactor : 0;
//     }

//   }

// }

// function clearCanvas() {
//   ctx.clearRect(0, 0, W, H);
// }

// const balls = [];

// canvas.addEventListener('click', function (event) {
//   const rect = this.getBoundingClientRect(),
//     x = event.clientX - rect.left,
//     y = event.clientY - rect.top;
//   balls.push(new Ball(x, y));
//   if (balls.length > 5) {
//     balls.shift();
//   }
// });


// function update() {
//   clearCanvas();

//   for (let i = 0, ball; ball = balls[i]; i++) {
//     ball.draw();
//     ball.update();
//   }

//   requestAnimationFrame(update);
// };
// update()


// ! Bookmarks
const bookmarkRootLevel = document.getElementById("bookmark-root-level");
const bookmarkFirstLevel = document.getElementById("bookmark-first-level");

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function loadChildren(id) {
    chrome.bookmarks.getChildren(id, function (results) {
        if (!(results && results.length > 0)) {
            return;
        }
        if (document.getElementsByClassName("bookmarks-box-child") && document.getElementsByClassName("bookmarks-box-child").length == 1) {
            const removeNode = document.getElementsByClassName("bookmarks-box-child")[0];
            bookmarkRootLevel.removeChild(removeNode);
        }
        const node = document.createElement("DIV");
        node.className = "bookmarks-box-child";
        node.id = id;
        bookmarkRootLevel.appendChild(node);
        results.forEach(itemChild => {
            const childNode = itemChild.url ? document.createElement("A") : document.createElement("BUTTON");
            const textChildNode = document.createTextNode(itemChild.title);
            if (itemChild.url) {
                childNode.href = itemChild.url;
                childNode.title = itemChild.title;
                childNode.target = "_blank";
                childNode.classList = "my-10"
                childNode.style.display = "block";
            } else {
                childNode.id = itemChild.id;
                childNode.addEventListener('click', function () {
                    loadChildren(itemChild.id)
                })
            }
            childNode.appendChild(textChildNode);
            node.appendChild(childNode)

        })
    })
}
function init(){
    chrome.bookmarks.getChildren('0', function (results) {
        if (!(results && results.length > 0)) {
            return;
        }
        results.forEach(item => {
            const node = document.createElement("BUTTON");
            node.className = "first-level";
            node.id = item.id;
            const textNode = document.createTextNode(item.title);
            node.appendChild(textNode);
            bookmarkFirstLevel.appendChild(node);
            node.addEventListener('click', function () {
                loadChildren(item.id)
            })
        });
    })
}
init();